#!/usr/bin/env sh

python3 \
  -m gunicorn \
  --worker-connections ${GUNICORN_WORKERS:=1} \
  --bind :${GUNICORN_PORT:=8000} \
  --preload \
  ts3_watcher.wsgi:application
