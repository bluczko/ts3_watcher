from django.urls import path

from . import views

app_name = "web_ui"
urlpatterns = (
    path("", views.IndexView.as_view(), name="index"),
    path("top/", views.TopUsersByTimeView.as_view(), name="top_alltime"),
    path("top/<int:year>/", views.TopUsersByTimeView.as_view(scope="year"), name="top_yearly"),
    path("top/<int:year>/<int:month>/", views.TopUsersByTimeView.as_view(scope="month"), name="top_monthly"),
    path("top/<int:year>/<int:month>/<int:day>/", views.TopUsersByTimeView.as_view(scope="day"), name="top_daily"),
)
