(() => {
    const LS_THEME = 'themeMode';
    const T_DARK = 'dark';
    const T_LIGHT = 'light';

    const switcher = document.querySelector('#theme-switcher');

    function setTheme(mode, updateSwitch = false) {
        document
            .querySelector('html')
            .setAttribute('data-bs-theme', mode);

        localStorage.setItem(LS_THEME, mode);

        if (updateSwitch) {
            switcher.checked = mode === T_LIGHT;
        }
    }

    setTheme((() => {
        let storedPref = localStorage.getItem(LS_THEME);

        if (storedPref === null) {
            let systemPref = window.matchMedia('(prefers-color-scheme: dark)').matches;
            return systemPref ? T_LIGHT : T_DARK;
        }

        return storedPref;
    })(), true);

    switcher.addEventListener('change', (e) => setTheme(e.target.checked ? T_LIGHT : T_DARK));
})();
