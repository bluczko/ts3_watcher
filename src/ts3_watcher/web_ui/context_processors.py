from django.conf import settings
from django.core.cache import cache

from ts3_watcher.core import consts


def server_info(_):
    """Append variables used every template extended from web_ui/_base.html."""
    return dict(
        ts3=dict(
            server_info=cache.get(consts.TS3_SERVER_INFO_CACHE_KEY),
            public_addr=f"ts3server://{settings.TS3_PUBLIC_ADDR}" if settings.TS3_PUBLIC_ADDR else None,
        ),
    )
