from django.apps import AppConfig


class WebUiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ts3_watcher.web_ui"
