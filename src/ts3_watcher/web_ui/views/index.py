from datetime import datetime

from django.conf import settings
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from ts3_watcher.core.query import ts3_connection
from ts3_watcher.utils import channel_tree


class IndexView(TemplateView):
    template_name = "web_ui/index.html"

    @method_decorator(cache_page(settings.PAGE_CACHE_TIME))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.method == "OPTIONS":
            return context

        with ts3_connection() as query:
            # Get lists of channels and active clients
            channels = query.exec_("channellist", "flags", "icon").parsed
            clients = query.exec_("clientlist", "away", "voice").parsed

            # Get every client from the database (active and inactive)
            db_client_count = query.exec_("clientdblist", "count").parsed[0]["count"]
            db_clients = query.exec_("clientdblist", duration=db_client_count).parsed

        # Filter out server query clients
        clients = [cl for cl in clients if cl["client_type"] == "0"]

        # Filter out clients that are already online
        db_clients = [
            dbcl for dbcl in db_clients
            if not any(cl["client_database_id"] == dbcl["cldbid"] for cl in clients)
        ]

        # Sort clients by last connected time
        db_clients = sorted(
            db_clients,
            key=lambda cl: datetime.fromtimestamp(int(cl["client_lastconnected"])),
            reverse=True,
        )

        context.update(
            timestamp=timezone.now(),
            channel_tree=channel_tree(channels, clients),
            recent_logins=db_clients[:settings.TS3_RECENT_LENGTH],
        )

        return context
