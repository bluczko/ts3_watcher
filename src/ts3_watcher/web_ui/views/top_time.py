import calendar
from datetime import datetime

from django.utils import timezone
from django.views.generic import TemplateView

from ts3_watcher.processing.models import UserConnection


class TopUsersByTimeView(TemplateView):
    template_name = "web_ui/top_time.html"
    scope = None  # overwritten in urlpatterns

    def get_date_range(self):
        year = self.kwargs.get("year")
        month = self.kwargs.get("month")
        day = self.kwargs.get("day")

        match self.scope:
            case "year":
                begin = datetime(year=year, month=1, day=1)
                end = datetime.combine(datetime(year=year, month=12, day=31), datetime.max.time())

            case "month":
                _, last_day = calendar.monthrange(year, month)
                begin = datetime(year=year, month=month, day=1)
                end = datetime.combine(datetime(year=year, month=month, day=last_day), datetime.max.time())

            case "day":
                begin = datetime(year=year, month=month, day=day)
                end = datetime.combine(begin, datetime.max.time())

            case _:
                return None, None

        begin = timezone.make_aware(begin)
        end = timezone.make_aware(end)

        return begin, end

    def get_date_scroll_iters(self, lb: datetime, ub: datetime) -> dict:
        year = self.kwargs.get("year")
        month = self.kwargs.get("month")

        iters = {
            "years": range(lb.year, ub.year + 1),
        }

        if self.scope in ("year", "month", "day"):
            lbm = lb.month if lb.year == year else 1
            ubm = ub.month if ub.year == year else 12
            iters["months"] = range(lbm, ubm + 1)

        if self.scope in ("month", "day"):
            _, last_day = calendar.monthrange(ub.year, ub.month)
            lbd = lb.day if lb.year == year and lb.month == month else 1
            ubd = ub.day if ub.year == year and ub.month == month else last_day
            iters["days"] = range(lbd, ubd + 1)

        return iters

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        qs = UserConnection.objects.all()

        global_bounds = qs.aggregate_dt_boundaries()
        scope_bounds = global_bounds

        scroll_iters = self.get_date_scroll_iters(
            global_bounds["since"],
            global_bounds["until"]
        )

        scope_begin, scope_end = self.get_date_range()

        if scope_begin and scope_end:
            qs = qs.filter_between(scope_begin, scope_end)
            scope_bounds = qs.aggregate_dt_boundaries()

        top_users = qs.group_top_users()[:10]

        user_ids = [top["user_id"] for top in top_users]

        nicknames_qs = qs.group_last_user_nicknames(user_ids)
        nicknames = {user["user_id"]: user["user_nickname"] for user in list(nicknames_qs)}

        for top in top_users:
            top["user_nickname"] = nicknames.get(top.get("user_id"))
            top["total_hours"] = round(top["total_connection_time"].total_seconds() / 3600)

        context.update(
            scope=self.scope,
            global_bounds=global_bounds,
            scope_bounds=scope_bounds,
            scroll_iters=scroll_iters,
            scope_begin=scope_begin,
            scope_end=scope_end,
            top_users=top_users,
        )

        return context
