from datetime import datetime

from django import template
from django.utils.timezone import make_aware

register = template.Library()


@register.filter
def channel_icon(channel: dict) -> str:
    is_default = channel.get("channel_flag_default", "0") == "1"
    return "i-ch-default" if is_default else "i-ch-regular"


@register.filter
def client_icon(client: dict) -> str:
    if client.get("client_away", "0") == "1" or client.get("client_output_muted", "0") == "1":
        return "i-cl-no-out"

    if client.get("client_input_muted", "0") == "1":
        return "i-cl-no-in"

    return "i-cl-regular"


@register.filter
def ts_date(timestamp: str):
    return make_aware(datetime.fromtimestamp(int(timestamp)))
