from django.db.models import QuerySet, Manager, F, Sum, Q, Min, Max


class UserConnectionQuerySet(QuerySet):
    def filter_at(self, dt) -> QuerySet:
        """Filter connections that were active at a given datetime."""
        return self.filter(
            connected_at__gte=dt,
            disconnected_at__lt=dt,
        )

    def filter_between(self, start_dt, end_dt) -> QuerySet:
        """Filter connections that were active between two datetimes (inclusive on both ends)."""
        return self.filter(
            connected_at__gte=start_dt,
            disconnected_at__lte=end_dt
        )

    def annotate_connection_time(self) -> QuerySet:
        """Annotate connection time as timedelta."""
        return self.annotate(
            connection_time=F("disconnected_at") - F("connected_at")
        )

    def annotate_total_connection_time(self) -> QuerySet:
        """Annotate total connection time as timedelta."""
        return self.annotate(
            total_connection_time=Sum(F("connection_time"))
        )

    def group_top_users(self) -> QuerySet:
        """Group by user_id and sum connection time, then order by connection time."""
        return (
            self.annotate_connection_time()
            .values("user_id")
            .annotate_total_connection_time()
            .order_by("-total_connection_time")
            .values("user_id", "total_connection_time")
        )

    def group_last_user_nicknames(self, user_ids: [int]) -> QuerySet:
        """Group by user_id and select the last user_nickname."""
        return (
            self.filter(user_id__in=user_ids)
            .values("user_id")
            .distinct()
            .order_by("-disconnected_at")
            .values("user_id", "user_nickname")
        )

    def aggregate_dt_boundaries(self):
        """Annotate the date boundaries of the connection."""
        return self.aggregate(
            since=Min("connected_at"),
            until=Max("disconnected_at"),
        )


class UserConnectionManager(Manager.from_queryset(UserConnectionQuerySet)):
    pass
