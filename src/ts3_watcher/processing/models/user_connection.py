from django.db import models

from ts3_watcher.processing.querysets import UserConnectionManager


class UserConnection(models.Model):
    user_id = models.IntegerField(db_index=True)
    user_nickname = models.CharField(max_length=255)
    connected_at = models.DateTimeField(db_index=True)
    disconnected_at = models.DateTimeField(db_index=True)

    objects = UserConnectionManager()

    def __str__(self):
        return f"UserConnection({self.user_id}, {str(self.connected_at)}, {str(self.disconnected_at)})"
