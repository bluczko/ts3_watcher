import os
import re
from datetime import datetime, timedelta

from django.conf import settings
from django.core.management import BaseCommand
from django.utils import timezone

from ts3_watcher.processing.models import UserConnection


def date_from_filename(file_name: str) -> datetime:
    _, ymd, _, h, m, second_ms, _ = (
        os.path.basename(file_name)
        .replace(".log", "")
        .split("_")
    )

    y, m, d = ymd.split("-")
    s, ms = second_ms.split(".")

    return datetime(
        year=int(y),
        month=int(m),
        day=int(d),
        hour=int(h),
        minute=int(m),
        second=int(s),
        microsecond=int(ms),
    )


def should_parse_line(log_line: str) -> bool:
    return (
        "client connected" in log_line or
        "client disconnected" in log_line
    )


def list_log_files(logs_root_dir, virtual_id=None, last_log_dt=None):
    file_names = sorted(os.listdir(logs_root_dir))

    if virtual_id is not None:
        file_names = [
            fn
            for fn in file_names
            if fn.endswith(f"_{virtual_id}.log")
        ]

    if last_log_dt is not None:
        dt = timezone.make_naive(last_log_dt)
        i = 0

        # Find the first log file that may contain newer data
        for fn in file_names:
            i += 1

            if date_from_filename(fn) > dt:
                break

        if i < len(file_names) - 1:
            file_names = file_names[i:]
        else:
            file_names = file_names[-1:]

    return [os.path.join(logs_root_dir, fn) for fn in file_names]


def iter_log_lines(log_file_path):
    """Iterate over the lines in file and yield only those related to user connections."""

    with open(log_file_path, "rt") as log_file:
        for line in log_file:
            if should_parse_line(line):
                yield line


def parse_connection_data(log_line: str) -> tuple:
    """Split the line into parts and parse its data."""
    dt, _, _, _, content = log_line.split("|", 4)
    conn_state = False if "disconnected" in log_line else True
    dt = datetime.strptime(dt, "%Y-%m-%d %H:%M:%S.%f")

    matched_user_data = re.search(r"'(.+)'\(id:(\d+)\)", content)

    if not matched_user_data:
        raise ValueError(f"Could not parse malformed connection log: \"{log_line}\"")

    user_nickname, user_id = matched_user_data.groups()

    if timezone.is_naive(dt):
        dt = timezone.make_aware(dt)

    return dt, conn_state, int(user_id), user_nickname


def period_seconds(start: datetime, end: datetime) -> float:
    return (end - start).total_seconds()


def user_conn_midnight_split(
    user_id: int,
    user_nickname: str,
    connected_at: datetime,
    disconnected_at: datetime,
) -> [UserConnection]:
    """Returns one or more UserConnection objects, split between days."""

    assert connected_at < disconnected_at, "Connection must start before it ends"

    conn_slices = []

    # Handle the simple case - connect and disconnect on the same day
    if connected_at.date() == disconnected_at.date():
        conn_slices.append(
            UserConnection(
                user_id=user_id,
                user_nickname=user_nickname,
                connected_at=connected_at,
                disconnected_at=disconnected_at,
            )
        )

    # Handle the case when the connection time spans over more than one day
    else:
        # Initial slice: at connection date
        slice_begin = connected_at

        # Initial end: just before midnight
        slice_end = timezone.make_aware(datetime.combine(connected_at.date(), datetime.max.time()))

        while True:
            # Create a sliced connection part
            conn_slices.append(
                UserConnection(
                    user_id=user_id,
                    user_nickname=user_nickname,
                    connected_at=slice_begin,
                    disconnected_at=slice_end,
                )
            )

            # Break the loop if we reached the end of the connection
            if slice_end == disconnected_at:
                break

            # Next slice begin: midnight next day
            slice_begin = timezone.make_aware(
                datetime.combine(slice_end.date() + timedelta(days=1), datetime.min.time())
            )

            # Next slice end: moment just before midnight or the end of the connection (whatever comes first)
            slice_end = min(
                disconnected_at,
                timezone.make_aware(
                    datetime.combine(slice_begin.date(), datetime.max.time())
                )
            )

    return conn_slices


class Command(BaseCommand):
    help = "Collect user connection data from server"

    def add_arguments(self, parser):
        parser.add_argument(
            "--logs-dir",
            type=str,
            default=settings.TS3_LOGS_DIR,
            help="Path to the directory with TS3 logs"
        )

        parser.add_argument(
            "--virtual-id",
            type=int,
            default=settings.TS3_VIRTUAL_ID,
            help="Virtual server ID (TS3_VIRTUAL_ID by default)"
        )

        parser.add_argument(
            "--full-import",
            action="store_true",
            help="If present, clear data and reimport everything, otherwise import just the new lines"
        )

        parser.add_argument(
            "--list-files",
            action="store_true",
            help="If present, do not save the connections to the database"
        )

    def handle(self, *args, logs_dir: str, virtual_id: int, full_import: bool, list_files: bool, **options):
        if full_import:
            UserConnection.objects.all().delete()

        last_conn = (
            UserConnection.objects.all()
            .order_by("disconnected_at")
            .last()
        )

        last_date = last_conn.disconnected_at if last_conn else None

        server_state = {}
        conn_buf = []
        total_added = 0

        for log_file in list_log_files(logs_dir, virtual_id, last_date):
            for log_line in iter_log_lines(log_file):
                dt, state_change, user_id, user_nickname = parse_connection_data(log_line)

                if last_date and dt < last_date:
                    continue

                if state_change and user_id not in server_state:  # user connected
                    server_state[user_id] = dt

                elif not state_change and user_id in server_state:  # user disconnected
                    conn_buf += user_conn_midnight_split(
                        user_id=user_id,
                        user_nickname=user_nickname,
                        connected_at=server_state[user_id],
                        disconnected_at=dt,
                    )

                    del server_state[user_id]

                    # Save connections in bulk
                    if len(conn_buf) >= 1000:
                        UserConnection.objects.bulk_create(conn_buf)
                        total_added += len(conn_buf)
                        conn_buf = []

        # Save the rest of the connections
        if len(conn_buf) > 0:
            UserConnection.objects.bulk_create(conn_buf)
            total_added += len(conn_buf)

        msg = "No new user connections found"

        if total_added > 0:
            msg = f"Successfully imported {total_added} user connections"

        self.stdout.write(msg)
