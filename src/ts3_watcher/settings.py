import os.path
from urllib.parse import urlparse

from environ import Env
from pathlib import Path

from django.utils.translation import gettext_lazy as _

BASE_DIR = Path(__file__).resolve().parent

env = Env()
env.read_env(str(BASE_DIR.parent / ".env"))

DEBUG = env("DEBUG", default=False)
SECRET_KEY = env("SECRET_KEY", default="no_secret_key")
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default=[])

LIB_APPS = (
    "django.contrib.humanize",
    "django.contrib.messages",
    "django.contrib.staticfiles",
)

PROJECT_APPS = (
    "ts3_watcher.processing",
    "ts3_watcher.web_ui",
)

INSTALLED_APPS = LIB_APPS + PROJECT_APPS

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "ts3_watcher.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.template.context_processors.i18n",
                "ts3_watcher.web_ui.context_processors.server_info",
            ],
        },
    },
]

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
    }
}

DATABASES = {
    "default": env.db("DATABASE_URL", default="sqlite://:memory:"),
}

WSGI_APPLICATION = "ts3_watcher.wsgi.application"
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

PAGE_CACHE_TIME = env("PAGE_CACHE_TIME", default=5)

LANGUAGE_CODE = env("LANGUAGE_CODE", default="en")
LANGUAGES = (
    ("en", _("English")),
    ("pl", _("Polish")),
)

LOGS_DIR = env("LOGS_DIR", default=str(BASE_DIR.parent / "logs"))
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_true": {"()": "django.utils.log.RequireDebugTrue"},
    },
    "formatters": {
        "simple": {
            "format": "%(levelname)s %(name)s" "\n%(message)s"
        },
        "verbose": {
            "format": (
                "%(levelname)-8s %(asctime)s %(name)s"
                "\n%(pathname)s:%(lineno)s"
                "\n%(message)s"
                "\n"
            ),
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
        "file": {
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "filename": os.path.join(LOGS_DIR, "django.log"),
        },
    },
    "loggers": {
        "django": {
            "propagate": True,
        },
    },
    "root": {
        "handlers": ["console", "file"],
    },
}

USE_I18N = True
USE_TZ = True
TIME_ZONE = env("TIME_ZONE", default="UTC")

STATIC_URL = env("STATIC_URL", default="static/")
STATIC_ROOT = env("STATIC_ROOT", default=str(BASE_DIR.parent / "static"))
STATICFILES_DIRS = [
    str(BASE_DIR / "static"),
    str(BASE_DIR / "web_ui" / "static"),
]

TS3_URL = env("TS3_URL", default="ts3://serverquery:password@localhost:10011")
TS3_PUBLIC_ADDR = env("TS3_PUBLIC_ADDR", default=None)
TS3_PUBLIC_NAME = env("TS3_PUBLIC_NAME", default="")
TS3_RECENT_LENGTH = env("TS3_RECENT_LENGTH", default=10)
TS3_VIRTUAL_ID = env("TS3_VIRTUAL_ID", default=1)
TS3_LOGS_DIR = env("TS3_LOGS_DIR", default=str(BASE_DIR.parent / "ts3_logs"))
TS3_SERVER_INFO_CACHE_TIME = env("TS3_SERVER_INFO_CACHE_TIME", default=3600)
