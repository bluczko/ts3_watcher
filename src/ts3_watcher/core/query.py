from django.conf import settings
from django.core.cache import cache
from ts3.query import TS3ServerConnection

from ts3_watcher.core import consts


def ts3_connection() -> TS3ServerConnection:
    # Connect to the TS3 server and select the virtual server
    conn = TS3ServerConnection(settings.TS3_URL)
    conn.exec_("use", sid=settings.TS3_VIRTUAL_ID)

    # Collect server info if not cached
    if not cache.get(consts.TS3_SERVER_INFO_CACHE_KEY):
        cache.set(
            consts.TS3_SERVER_INFO_CACHE_KEY,
            conn.exec_("serverinfo").parsed[0],
        )

    return conn
