def channel_tree(channel_list: list, client_list: list, parent_id=None) -> list:
    if parent_id is None:
        parent_id = "0"

    channels = []

    for channel in [c for c in channel_list if c["pid"] == parent_id]:
        clients = [c for c in client_list if c["cid"] == channel["cid"]]
        subchannels = channel_tree(channel_list, client_list, channel["cid"])

        if clients or subchannels:
            channels.append(dict(
                **channel,
                clients=clients,
                subchannels=subchannels,
            ))

    return channels
