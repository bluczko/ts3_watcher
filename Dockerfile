FROM python:3.12-slim AS build

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false

WORKDIR /app

RUN apt-get update \
    && apt-get install -y \
      gettext \
      python3-dev \
      build-essential \
      default-libmysqlclient-dev \
      pkg-config \
    && rm -Rf /usr/share/doc \
    && rm -Rf /usr/share/man \
    && apt-get autoclean \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && pip install poetry

RUN mkdir -p /app/static /app/logs /app/ts3_logs

COPY /src /app
RUN poetry install \
    && python manage.py compilemessages

FROM python:3.12-slim AS runtime

ENV GUNICORN_PORT=8000 \
    GUNICORN_WORKERS=1

WORKDIR /app

COPY --from=build /usr/local/lib/python3.12/site-packages /usr/local/lib/python3.12/site-packages
COPY --from=build /app /app

VOLUME ["/app/static"]
CMD ["sh", "./docker-entrypoint.sh"]
